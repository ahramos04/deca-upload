

resource "aws_security_group" "bastion" {
  name          = "${var.bastion_sg_name}"
  description   = "Bastion Server"
  vpc_id        = "${var.vpc_id}"
  
  ingress {
   from_port    = 22
   to_port      = 22
   protocol     = "tcp"
   cidr_blocks  = ["0.0.0.0/0"] 
  }
  
  ingress {
   from_port    = 80
   to_port      = 80
   protocol     = "tcp"
   cidr_blocks  = ["0.0.0.0/0"] 
  }
  
  ingress {
   from_port    = 443
   to_port      = 443
   protocol     = "tcp"
   cidr_blocks  = ["0.0.0.0/0"] 
  }
  
  egress {
   from_port    = 0
   to_port      = 0
   protocol     = "-1"
   cidr_blocks  = ["0.0.0.0/0"]
  }
 tags{
   Name         = "${var.bastion_sg_name}"
   Environment  = "${var.environment}"
  }

}

###Datasync

resource "aws_security_group" "datasync" {
  name          = "${var.datasync_sg_name}"
  description   = "Datasync Instance"
  vpc_id        = "${var.vpc_id}"
  
  ingress {
   from_port    = 22
   to_port      = 22
   protocol     = "tcp"
   security_groups  = ["${aws_security_group.bastion.id}"]
  }
  
  ingress {
   from_port    = 80
   to_port      = 80
   protocol     = "tcp"
   cidr_blocks  = ["0.0.0.0/0"] 
  }
  
  ingress {
   from_port    = 443
   to_port      = 443
   protocol     = "tcp"
   cidr_blocks  = ["0.0.0.0/0"] 
  }
  
  ingress {
   from_port    = 123
   to_port      = 123
   protocol     = "tcp"
   cidr_blocks  = ["0.0.0.0/0"] 
  }
  
  ingress {
   from_port    = 2049
   to_port      = 2049
   protocol     = "tcp"
   cidr_blocks  = ["0.0.0.0/0"] 
  }
  
  ingress {
   from_port    = 139
   to_port      = 139
   protocol     = "tcp"
   cidr_blocks  = ["0.0.0.0/0"] 
  }
  
  ingress {
   from_port    = 445
   to_port      = 445
   protocol     = "tcp"
   cidr_blocks  = ["0.0.0.0/0"] 
  }  
   
  
  egress {
   from_port    = 0
   to_port      = 0
   protocol     = "-1"
   cidr_blocks  = ["0.0.0.0/0"]
  }
 tags{
   Name         = "${var.datasync_sg_name}"
   Environment  = "${var.environment}"
  }

}

###Prestashop

resource "aws_security_group" "prestashop" {
  name          = "${var.prestashop_sg_name}"
  description   = "Prestashop Security Group"
  vpc_id        = "${var.vpc_id}"
  
  ingress {
   from_port    = 22
   to_port      = 22
   protocol     = "tcp"
   security_groups  = ["${aws_security_group.bastion.id}"]
  }
  
  ingress {
   from_port    = 80
   to_port      = 80
   protocol     = "tcp"
   cidr_blocks  = ["0.0.0.0/0"] 
  }
  
  ingress {
   from_port    = 443
   to_port      = 443
   protocol     = "tcp"
   cidr_blocks  = ["0.0.0.0/0"] 
  }
  
  egress {
   from_port    = 0
   to_port      = 0
   protocol     = "-1"
   cidr_blocks  = ["0.0.0.0/0"]
  }
 tags{
   Name         = "${var.bastion_sg_name}"
   Environment  = "${var.environment}"
  }

}

###RDS

resource "aws_security_group" "rds" {
   name          = "${var.rds_sg_name}"
   description   = "RDS Security Group"
   vpc_id        = "${var.vpc_id}"

   ingress{
     from_port   = 3306
     to_port     = 3306
     protocol    = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
 
  }
   
   egress {
    from_port    = 0
    to_port      = 0
    protocol     = "-1"
    cidr_blocks  = ["0.0.0.0/0"]
   }
  
  tags {
   Name          = "${var.rds_sg_name}"
   Environment   = "${var.environment}"
  }

}
/*
###RFID

resource "aws_security_group" "rfid" {
  name          = "${var.rfid_sg_name}"
  description   = "RFID Notification"
  vpc_id        = "${var.vpc_id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["158.140.143.201/32"]
  }
  
    ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["158.140.143.201/32"]
  }
  
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  ingress {
    from_port   = 8081
    to_port     = 8081
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags{
   Name         = "${var.rfid_sg_name}"
   Environment  = "${var.environment}"
  }
}


###TH CUSTOMER SATISFACTION

resource "aws_security_group" "th_customer_satisfaction" {
  name          = "${var.th_sg_name}"
  description   = "till-review"
  vpc_id        = "${var.vpc_id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["158.140.143.201/32"]
  }
  
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
    
  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["158.140.143.201/32"]
  }
    
  ingress {
    from_port   = 8081
    to_port     = 8081
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags{
   Name         = "${var.th_sg_name}"
   Environment  = "${var.environment}"
  }
}

### ELAB PROD


resource "aws_security_group" "elab_prod" {
  name          = "${var.elab_prod_name}"
  description   = "Prestashop 1.7 2018-11-08"
  vpc_id        = "${var.vpc_id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["158.140.143.201/32","101.78.80.18/32","116.87.17.167/32"]
  }
  
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
    
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
    


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags{
   Name         = "${var.elab_prod_name}"
   Environment  = "${var.environment}"
  }
}

###LBN MANAGENEMENT

resource "aws_security_group" "lbn_mgmt" {
  name          = "${var.lbn_mgt_name}"
  description   = "Prestashop 1.7 2018-11-08"
  vpc_id        = "${var.vpc_id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["172.21.192.4/32","172.21.192.6/32","89.107.168.195/32"]
  }
  
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["172.21.192.4/32","27.74.241.237/32"]
  }
    
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["172.21.192.4/32","27.74.241.237/32"]
  }
    
  ingress {
    from_port   = 58081
    to_port     = 58081
    protocol    = "tcp"
    cidr_blocks = ["172.21.192.4/32"]
  }
  
  ingress {
    from_port   = 135
    to_port     = 139
    protocol    = "tcp"
    cidr_blocks = ["172.21.192.4/32"]
  }
  
  ingress {
    from_port   = 445
    to_port     = 445
    protocol    = "tcp"
    cidr_blocks = ["172.21.192.4/32"]
  }
  
  ingress {
    from_port   = 1024
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["172.21.192.4/32"]
  }
  
  ingress {
    from_port   = 10050
    to_port     = 10050
    protocol    = "tcp"
    cidr_blocks = ["172.21.192.5/32"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags{
   Name         = "${var.lbn_mgt_name}"
   Environment  = "${var.environment}"
  }
}


###DECASG PREPROD

resource "aws_security_group" "decasg_preprod" {
  name          = "${var.decasg_preprod_name}"
  description   = "Ecommerce Prestashop Preprod 1.7"
  vpc_id        = "${var.vpc_id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["158.140.143.201/32","101.78.80.18/32","116.87.17.167/32"]
  }
  
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
    
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
    


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags{
   Name         = "${var.decasg_preprod_name}"
   Environment  = "${var.environment}"
  }
}

###DECASHOP ELASTIC CACHE

resource "aws_security_group" "decashop_elastic_cache" {
  name          = "${var.decashop_elastic_name}"
  description   = "Security Group for Decashop Elastic Cache"
  vpc_id        = "${var.vpc_id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["172.31.0.0/20"]
  }
  
  ingress {
    from_port       = 0
    to_port         = 65535
    protocol        = "tcp"
    security_groups = ["${aws_security_group.decasg_preprod.id}","${aws_security_group.elab_prod.id}"]
  }
   
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags{
   Name         = "${var.decashop_elastic_name}"
   Environment  = "${var.environment}"
  }
}

*/


###EFS

resource "aws_security_group" "efs_sg" {
  name          = "${var.efs_sg_name}"
  description   = "Allow EFS inbound traffic"
  vpc_id        = "${var.vpc_id}"

  ingress {
    from_port   = 2049
    to_port     = 2049
    protocol    = "tcp"
	cidr_blocks = ["0.0.0.0/0"]
    #
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags{
   Name         = "${var.efs_sg_name}"
   Environment  = "${var.environment}"
  }
}


###EKS SG 

resource "aws_security_group" "worker_group_mgmt_one" {
  #name_prefix = "worker_group_mgmt_one"
  name         = "${var.eks_worker_mgmt_one_name}"
  vpc_id      = "${var.vpc_id}"
  description = "Allow all unprivileged ports group one"

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"

    cidr_blocks = ["${var.eks_cidr}"]
  }

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"

    cidr_blocks = ["${var.eks_cidr}"]
  }
 tags{
  Name         = "${var.eks_worker_mgmt_one_name}"
  environment  = "${var.environment}" 
 }
}

resource "aws_security_group" "worker_group_mgmt_two" {
  #name_prefix = "worker_group_mgmt_two"
  name         = "${var.eks_worker_mgmt_two_name}"
  vpc_id      = "${var.vpc_id}"
  description = "Allow all unprivileged ports group two"

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"

    cidr_blocks = ["${var.eks_cidr}"]
  }

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"

    cidr_blocks = ["${var.eks_cidr}"]
  }
  tags{
    Name         = "${var.eks_worker_mgmt_two_name}"
    environment  = "${var.environment}" 
 }
}

resource "aws_security_group" "all_worker_mgmt" {
  #name_prefix = "all_worker_mgmt"
  name         = "${var.eks_all_worker_mgmt_name}"
  vpc_id      = "${var.vpc_id}"
  description = "Allow SSH HTTP HTTPS ports from other nodes"

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = ["${var.eks_cidr}"]
  }
 tags{
  Name         = "${var.eks_all_worker_mgmt_name}"
  environment  = "${var.environment}" 
 }
}

###KMS ENDPOINT

resource "aws_security_group" "kms_endpoint_sg"{
  name        = "${var.kms_endpoint_name}"
  description = "Allow kms endpoint inbound traffic port"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.84.0.0/16"]
  }


  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags{
   Name = "${var.kms_endpoint_name}"
   Environment = "${var.environment}"
  }
}

###EC2 ENDPOINT

resource "aws_security_group" "ec2_endpoint_sg"{
  name        = "${var.ec2_endpoint_name}"
  description = "Allow ec2 endpoint inbound traffic port"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.84.0.0/16"]
  }


  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags{
   Name        = "${var.ec2_endpoint_name}"
   Environment = "${var.environment}"
  }
}

###ALB

resource "aws_security_group" "alb_sg"{
  name        = "${var.alb_sg_name}"
  description = "ALB inbound traffic port"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags{
   Name        = "${var.alb_sg_name}"
   Environment = "${var.environment}"
  }
}