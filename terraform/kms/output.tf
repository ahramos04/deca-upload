output "kms_key_rds" {
 value = "${aws_kms_alias.rds-alias.arn}"
}

output "kms_key_efs" {
 value = "${aws_kms_alias.efs-alias.arn}"
}

