variable "ec2_instance_type"{

}

variable "ec2_sg" {
 default = []
}

variable "ec2_subnet_id" {

}

variable "environment" {

}

variable "ec2_key_name" {

}

variable "efs_mount_arn" {

}

variable "efs_subnet_arn" {

}

variable "security_group_arn" {
 default = []
}

variable "dr_efs_hostname" {

}

variable "name" {

}

variable "efs_ip_address" {
  default  = []
 
}
