

data "aws_kms_alias" "kr_rds" {
  name = "alias/${var.dr_kms_name}"
}

data "aws_security_group" "dr_rds_sg" {
 filter {
  name   = "tag:Name"
  values = ["${var.rds_dr_sg_name}"] 
 }
}


data "aws_subnet" "dr_data_az1" {
 filter {
  name = "tag:Name"
  values = ["${var.dr_data_subnet_az1_name}"]
 }
}

data "aws_subnet" "dr_data_az2" {
 filter {
  name = "tag:Name"
  values = ["${var.dr_data_subnet_az2_name}"]
 }
}



data "aws_subnet" "dr_data_az3" {
 filter {
  name = "tag:Name"
  values = ["${var.dr_data_subnet_az3_name}"]
 }
}

/*

resource "aws_rds_cluster" "aurora_crr_cluster" {
    
 cluster_identifier            = "${var.cluster_identifier}"
 database_name                 = "${var.database_name}"
 master_username               = ""
 master_password               = ""
 engine_version                = "${var.engine_version}"
 storage_encrypted             = true
 vpc_security_group_ids        = ["${data.aws_security_group.dr_rds_sg.id}"]
 kms_key_id                    = "${data.aws_kms_alias.kr_rds.arn}"
 db_subnet_group_name          = "${aws_db_subnet_group.dr_db_subnet_grp.id}"
 backup_retention_period       = "${var.backup_retention}"
 source_region                 = "${var.source_region}"
 apply_immediately             = true
 replication_source_identifier = "${var.rds_cluster_arn}"

 tags {
	Name         = "${var.tag_name}"
	Environment  = "${var.environment}"
 }

 lifecycle {
	create_before_destroy = true
	ignore_changes         = ["kms_key_id","availability_zones"]
 }

}
    
resource "aws_rds_cluster_instance" "aurora_crr_cluster_instance" {

 cluster_identifier    = "${aws_rds_cluster.aurora_crr_cluster.id}"       
 identifier            = "${var.cluster_identifier}-1"
 instance_class        = "${var.instance_class}"
 engine_version        = "${var.engine_version}"
 publicly_accessible   = false
 apply_immediately     = true
 promotion_tier        = "15"

 tags {
	Name         = "${var.tag_name}"
	Environment  = "${var.environment}"
 }

 lifecycle {
	create_before_destroy = true
 }

}

*/

### RDS SUBNET

resource "aws_db_subnet_group" "dr_db_subnet_grp"{
 name = "${lower(var.rds_dr_subnet_name)}"
 subnet_ids = ["${data.aws_subnet.dr_data_az1.id}","${data.aws_subnet.dr_data_az2.id}","${data.aws_subnet.dr_data_az3.id}"]
 tags {
  name = "${var.rds_dr_subnet_name}"
  environment = "${var.environment}"
 }
}

