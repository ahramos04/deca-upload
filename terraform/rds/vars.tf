data "aws_availability_zones" "db_aws_zones" {}

/*
variable "db_instance_number"{
 description = "Number of Database Instance"
 #default = "1"
}


variable "bucket_prefix" {
 description = "S3 Bucket prefix"
}
*/


variable "prefix" {
 description = "RDS Prefix"
}

variable "environment"{
 description = "Object environment"
}



variable "allocated_storage_size"{
 description = "Database Storage Size"
 default = 20
}

variable "max_allocated_storage" {
 description = "Autoscaling max storage"
 default = 1000
}

variable "db_storage_type"{
 description = "Database Storage Type Standard,gp2,io1"
 default = "gp2"
}

variable "db_az"{
 description = "Database Availability Zone"
 default = "ap-east-1a"
}

variable "db_username"{
 description = "Database Username"
}

variable "db_password"{
 description = "Database Password"

}


variable "rds_sg"{
 description = "RDS Security Group"
 type = "list" 
}


variable "rds_subnet_group"{
 description = "RDS Subnet Group"
 type = "list"
}

variable "multi_az"{
 description = "Multi Availability Zone for Database"
}

variable "skip_snapshot"{
 description = "Set false to refrain from being deleted with terraform destroy"
}

variable "db_identifier"{
 description = "Database description indentifier"

}

variable "kms_key"{
 description = "KMS Key for database"
 #default = []
}

variable "rds_subnet_name" {

}

variable "engine" {

}


variable "engine_version" {

}


variable "instance_class" {

}

variable "availability_zones" {
 default = []
}

variable "prefix_dns" {

}

