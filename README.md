# Decathlon

## IAM
We need to conform with the least privilege principle. The user `svc_devops_admin` although having full access to selected services will not have console access. Only Terraform and this repository will use this account.  
```
aws iam create-group --group-name svc_devops_group --profile deca  
aws iam create-group --group-name svc_devops_group_admin --profile deca  
aws iam add-user-to-group --group-name svc_devops_group --user-name svc_devops_sda --profile deca
aws iam add-user-to-group --group-name svc_devops_group_admin --user-name svc_devops_sda --profile deca 
aws iam attach-group-policy --group-name svc_devops_group --policy-arn "arn:aws:iam::aws:policy/AmazonS3FullAccess" --profile deca 
aws iam attach-group-policy --group-name svc_devops_group --policy-arn "arn:aws:iam::aws:policy/AmazonRDSFullAccess" --profile deca
aws iam attach-group-policy --group-name svc_devops_group --policy-arn arn:aws:iam::aws:policy/AmazonElasticFileSystemFullAccess --profile deca
aws iam attach-group-policy --group-name svc_devops_group --policy-arn arn:aws:iam::784571545063:policy/AmazonEKSFullAccess --profile deca
aws iam attach-group-policy --group-name svc_devops_group --policy-arn "arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess" --profile deca  
aws iam attach-group-policy --group-name svc_devops_group --policy-arn "arn:aws:iam::aws:policy/AmazonVPCReadOnlyAccess" --profile deca  
aws iam attach-group-policy --group-name svc_devops_group --policy-arn "arn:aws:iam::aws:policy/AmazonRDSReadOnlyAccess" --profile deca  
aws iam attach-group-policy --group-name svc_devops_group --policy-arn "arn:aws:iam::aws:policy/AmazonRDSReadOnlyAccess"
aws iam attach-group-policy --group-name svc_devops_group_admin --policy-arn "arn:aws:iam::aws:policy/AWSCertificateManagerReadOnly" --profile deca
```

The user `svc_devops_ro` will have console access but it will be strictly read-only for selected services.
### Create `svc_devops_ro` with console access
```
aws iam create-user --user-name svc_devops_ro --profile deca  
export RO=veryhardpassword  
aws iam create-login-profile --user-name svc_devops_ro --password $RO --profile deca  
aws iam create-group --group-name svc_devops_ro_group --profile deca  
aws iam add-user-to-group --group-name svc_devops_ro_group --user-name svc_devops_ro --profile deca 
aws iam attach-group-policy --group-name svc_devops_ro_group --policy-arn "arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess" --profile deca  
aws iam attach-group-policy --group-name svc_devops_ro_group --policy-arn "arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess" --profile deca  
aws iam attach-group-policy --group-name svc_devops_ro_group --policy-arn "arn:aws:iam::aws:policy/AmazonVPCReadOnlyAccess" --profile deca  
aws iam attach-group-policy --group-name svc_devops_ro_group --policy-arn "arn:aws:iam::aws:policy/AmazonRDSReadOnlyAccess" --profile deca
aws iam attach-group-policy --group-name svc_devops_ro_group --policy-arn arn:aws:iam::784571545063:policy/AmazonEKSReadOnlyAccess --profile deca
aws iam attach-group-policy --group-name svc_devops_ro_group --policy-arn "arn:aws:iam::aws:policy/AmazonElasticFileSystemReadOnlyAccess" --profile deca  
```
## Packer
The base image that will be used is Amazon Linux 2 AMI 2.0.20190618 x86_64 HVM gp2  
`cd packer`  
`packer build bastion.json`  
`packer build devops.json` 
`packer build packer_ami.json` 

## Terraform
`cd terraform`
`terraform init`
`terraform plan`
`terraform apply`

### S3 bucket and key for terraform state
`aws s3api create-bucket --bucket decathlon-s3-au-dev-tfstate --create-bucket-configuration LocationConstraint=ap-southeast-2 --acl private --object-lock-enabled-for-bucket --profile deca`  
`aws s3api put-object --bucket decathlon-s3-au-dev-tfstate --acl bucket-owner-full-control --key terraform.tfstate --server-side-encryption aws:kms --profile deca`  

### Use S3 bucket and key for terraform
`terraform init -backend-config="bucket=decathlon-s3-au-dev-tfstate" -backend-config="key=terraform.tfstate" -backend-config="region=ap-southeast-2" -backend=true -force-copy -get=true -input=false`  

### Import IAM module to terraform for created users
`terraform import module.iam.aws_iam_user.admin svc_devops_admin`  
`terraform import module.iam.aws_iam_user.ro svc_devops_ro`  
