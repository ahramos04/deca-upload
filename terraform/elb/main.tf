
#
# NETWORK LOAD BALANCER
#


 # EXTERNAL


resource "aws_lb" "external_alb"{
 name               = "${var.external_alb_name}"
 internal           = false
 load_balancer_type = "application"
 security_groups    = ["${var.sec_grp}"]
 subnets            = ["${var.subnet}"]
 

 tags{
  Name = "${var.external_alb_name}"
  environment = "${var.environment}"
 }

}

