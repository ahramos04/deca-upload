


variable "environment"{
 description = "Environment Tag"
 default = "Production"
} 

variable "vpc_name" {
 description = "VPC Description name"
}

variable "vpc_subnet" {
 description = "VPC Subnet"
}


variable "igw_name" {

}

variable "eks_cluster_name" {

}

variable "eip_az1_name" {
  description = "Elastic IP name for AZ1"
}

variable "eip_az2_name" {
  description = "Elastic IP name for AZ2"
}

variable "eip_az3_name" {
  description = "Elastic IP name for AZ2"
}

variable "public_subnet" {
 description = "Production Public Subnet"
 default     = []
} 
 
variable "az1_subnet" {
 description = "Production AZ1 Subnet"
 default     = []
 
}

variable "az2_subnet" {
 description = "Production AZ2 Subnet"
 default     = []
 
}

variable "az3_subnet" {
 description = "Production AZ2 Subnet"
 default     = []
 
}

data "aws_availability_zones" "aws_zones" {}


variable "public_rt_table_name"{

}

variable "az1_rt_table_name"{

}

variable "az2_rt_table_name"{

}

variable "az3_rt_table_name"{

}

variable "az1_natgw_name" {

}

variable "az2_natgw_name" {

}

variable "az3_natgw_name" {

}

variable "weave_cidr" {

}

variable "weave_name" {

}

variable "public_subnet_names" {
 default = []
}

variable "az1_subnet_names" {
 default = []
}

variable "az2_subnet_names" {
 default = []
}

variable "az3_subnet_names" {
 default = []
}

variable "az1_subnet_az"{
 
}

variable "az2_subnet_az"{
 
}

variable "az3_subnet_az"{
 
}

variable "nacl_front_name" {

}

variable "nacl_app_name" {

}

variable "nacl_data_name" {

}

variable "nacl_pub_name" {

}

variable "dr_cidr_block" {

}

variable "peer_region" {

}

variable "peer_vpc_id" {

}