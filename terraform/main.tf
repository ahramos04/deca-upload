terraform {
  required_version = "= 0.11.14"
  
  backend "s3" {
    encrypt        = true
    bucket         = "decathlon-s3-au-dev-tfstate"
    region         = "ap-southeast-2"
    key            = "terraform.tfstate"
  }
}

###PROVIDERS

provider "aws" {
  profile = "deca"
  region  = "${var.region}"
  
}

###PROVIDER 2

provider "aws" {
 alias  = "dr"
 region = "ap-northeast-2"

}

###VARIABLES 


variable "region" {
  default = "ap-southeast-2"
}



variable "ssh_kp" {
 description = "remote ssh key pair"
 default = "DECA-AU-KP"
}

variable "prefix" {
 default = "AU-PREPROD"
}

variable "subnet_cidr" {
 default = "172.32.0.0/16"
}

variable "environment" {
 default  = "PREPROD"
}


variable "rds_engine_version" {

 default = "5.6.10a"
}

variable "rds_dr_region" {
  description = "Region for RDS DR replica"
  default = "ap-northeast-2"
}

variable "dr_rds_kms_key_name" {
  default = "KMS_KR_DECA_RDS"
}

variable "dr_prefix" {

 default = "KR-DR"
}

variable "dr_peer_region" {
 default  = "ap-northeast-2"
}



####TERRAFORM MAIN CONFIGURATION

module "iam" {
 source                 = "./iam"
 kms_admin              = "KMS_AU_DECA_ADMIN"
}

module "kms" {
 source                  = "./kms"
 region                  = "${var.region}"
 key_administrator       = "${module.iam.kms_admin}"
 rds_alias               = "alias/KMS_AU_DR_RDS"
 efs_alias               = "alias/KMS_AU_DECA_EFS"
}

/*
module "datasync" {
 source                 = "./datasync"
 ec2_instance_type      = "m4.xlarge"
 ec2_key_name           = "${var.ssh_kp}"
 ec2_sg                 = ["${module.sg.datasync_sg}"]
 ec2_subnet_id          = "${module.vpc.public_subnet_az1}"
 efs_mount_arn          = "${module.efs.efs_arn}"
 security_group_arn     = ["${module.sg.datasync_sg_arn}"]
 dr_efs_hostname        = "kr-dr-efs.decathlon.local"
 efs_subnet_arn         = "${module.vpc.front_az1_subnet_arn}" 
 name                   = "EC2-${var.prefix}-DATASYNC"
 environment            = "${var.preprod}"
}
*/

module "prestashop" {
  source                = "./prestashop"
  ami_name              = "AMI-${var.prefix}-PRESTASHOP"
  ami                   = "${var.prestashop_ami}"
  instance_count        = "2"
  instance_type         = "m5.xlarge"
  key_name              = "${var.ssh_kp}"
  sg                    = ["${module.sg.prestashop_sg}"]
  subnet                = ["${module.vpc.front_az1_subnet}","${module.vpc.front_az2_subnet}","${module.vpc.front_az3_subnet}"]
  name                  = "EC2-${var.prefix}-PRESTASHOP"
  prestashop_tg_name    = "TG-${var.prefix}-PRESTASHOP"
  external_alb          = "${module.lb.external_alb}"
  vpc_id                = "${module.vpc.vpc_id}"
  db_host               = "${module.rds.db_host}"
  db_user               = "deca_user"
  db_password           = "${var.rds_db_password}"
  efs_id                =  "${module.efs.efs_id}"
  front_subnet          = ["${module.vpc.front_az1_subnet}","${module.vpc.front_az2_subnet}"]
  asg_name              = "${var.prefix}-AUTOSCALING-PRESTASHOP"
  asg_min_size          = "2"
  asg_desired_capacity  = "2" 
  asg_max_size          = "2"
  environment           = "${var.preprod}"
}



module "vpc" {
  source                = "./vpc"
  vpc_name              = "VPC-${var.prefix}"
  vpc_subnet            = "${var.subnet_cidr}" 
  public_subnet         = ["172.32.0.0/24","172.32.1.0/24","172.32.2.0/24"]
  public_subnet_names   = ["SUB-${var.prefix}-PUBLIC-AZ1","SUB-${var.prefix}-PUBLIC-AZ2","SUB-${var.prefix}-PUBLIC-AZ3"]
  public_rt_table_name  = "RT-${var.prefix}-PUBLIC"
  az1_subnet            = ["172.32.3.0/24","172.32.6.0/24","172.32.9.0/24"]
  az2_subnet            = ["172.32.4.0/24","172.32.7.0/24","172.32.10.0/24"]
  az3_subnet            = ["172.32.5.0/24","172.32.8.0/24","172.32.11.0/24"]
  az1_subnet_names      = ["SUB-${var.prefix}-FRONT-AZ1","SUB-${var.prefix}-APP-AZ1","SUB-${var.prefix}-DATA-AZ1"]
  az2_subnet_names      = ["SUB-${var.prefix}-FRONT-AZ2","SUB-${var.prefix}-APP-AZ2","SUB-${var.prefix}-DATA-AZ2"]
  az3_subnet_names      = ["SUB-${var.prefix}-FRONT-AZ3","SUB-${var.prefix}-APP-AZ3","SUB-${var.prefix}-DATA-AZ3"]  
  az1_rt_table_name     = "RT-${var.prefix}-AZ1"
  az2_rt_table_name     = "RT-${var.prefix}-AZ2"
  az3_rt_table_name     = "RT-${var.prefix}-AZ3"
  az1_natgw_name        = "NAT-${var.prefix}-AZ1"
  az2_natgw_name        = "NAT-${var.prefix}-AZ2"
  az3_natgw_name        = "NAT-${var.prefix}-AZ3"
  az1_subnet_az         = "${var.region}a" 
  az2_subnet_az         = "${var.region}b"
  az3_subnet_az         = "${var.region}c"
  eip_az1_name          = "EIP-${var.prefix}-NATGW-AZ1"
  eip_az2_name          = "EIP-${var.prefix}-NATGW-AZ2"
  eip_az3_name          = "EIP-${var.prefix}-NATGW-AZ3"
  nacl_pub_name         = "NACL-${var.prefix}-PUB"
  nacl_front_name       = "NACL-${var.prefix}-FRONT"
  nacl_app_name         = "NACL-${var.prefix}-APP"
  nacl_data_name        = "NACL-${var.prefix}-DATA"
  igw_name              = "IGW-${var.prefix}-PREPROD"
  weave_cidr            = "172.32.30.0/23"
  weave_name            = "SUB-${var.prefix}-WEAVE"
  eks_cluster_name      = "EKS-${var.prefix}"
  dr_cidr_block         = "172.34.0.0/16" 
  peer_region           = "${var.dr_peer_region}"
  peer_vpc_id           = "vpc-05654fdb4be923bff"
  environment           = "${var.environment}"
}

/*
module "dr_vpc" {
  source                = "./dr_vpc"
  peer_az1_rtbl         = "RT-${var.dr_prefix}-AZ1"
  peer_az2_rtbl         = "RT-${var.dr_prefix}-AZ2"
  peer_az3_rtbl         = "RT-${var.dr_prefix}-AZ3"
  dr_vpc_peering_name   = "VPC-PEER-PREPROD-TO-DR"
  vpc_subnet            = "${var.subnet_cidr}"
 
  providers =  {
   aws = "aws.dr"
 }
}
*/

module "sg" {
  source                   = "./sg"
  vpc_id                   = "${module.vpc.vpc_id}"
  bastion_sg_name          = "SEC-${var.prefix}-DEC-BASTION"
  prestashop_sg_name       = "SEC-${var.prefix}-DECA-PRESTASHOP"
  rds_sg_name              = "SEC-${var.prefix}-RDS"
  efs_sg_name              = "SEC-${var.prefix}-EFS"
  alb_sg_name              = "SEC-${var.prefix}-ALB"
  eks_worker_mgmt_one_name = "SEC-${var.prefix}-WORKER-ONE-EKS"
  eks_worker_mgmt_two_name = "SEC-${var.prefix}-WORKER-TWO-EKS"
  eks_all_worker_mgmt_name = "SEC-${var.prefix}-ALL-WORKER-EKS"
  kms_endpoint_name        = "SEC-${var.prefix}-KMS-ENDPOINT"
  ec2_endpoint_name        = "SEC-${var.prefix}-EC2-ENDPOINT"
  datasync_sg_name         = "SEC-${var.prefix}-EC2-DATASYNC"
  eks_cidr                 = "${var.subnet_cidr}"
  environment              = "${var.environment}"
}



###RDS


module "rds"{
 source                 = "./rds"
 rds_subnet_group       = ["${module.vpc.data_subnet_az1}","${module.vpc.data_subnet_az2}","${module.vpc.data_subnet_az3}"]
 rds_sg                 = ["${module.sg.rds_sg}"]
 db_username            = "deca_user"   # Only numbers, letters, and _ are accepted
 db_password            = "${var.rds_db_password}"
 db_identifier          = "presta" # DBName must begin with a letter and contain only alphanumeric characters
 instance_class         = "db.t3.medium" 
 engine                 = "aurora"
 engine_version         = "${var.rds_engine_version}"
 availability_zones      = ["${var.region}a","${var.region}b"]
 rds_subnet_name        = "SUB-${var.prefix}-RDS"
 prefix                 = "RDS-${var.prefix}"
 prefix_dns             = "${var.prefix}"
 #bucket_prefix          = "S3-${var.prefix}"
 multi_az               = false
 max_allocated_storage  = 1000
 kms_key                = "${module.kms.kms_key_rds}"
 environment            = "${var.preprod}"
 skip_snapshot          = true        #set to false to prevent being deleted with terraform destroy 
}

/*

###RDS REPLICA DR

module "rds_replica" {
 source                  = "./rds_replica"
 #rds_cluster_arn         = "${module.rds.rds_arn}"
 #rds_cluster_arn         = "${module.rds.rds_arn}"
 cluster_identifier      = "rds-au-kr-presta"
 database_name           = "presta"
 instance_class          = "db.t3.medium"
 engine_version          = "${var.rds_engine_version}"
 source_region           = "${var.region}"
 dr_kms_name             = "${var.dr_rds_kms_key_name}"
 backup_retention        = "7"
 rds_dr_sg_name          = "SEC-${var.dr_prefix}-RDS"
 rds_dr_subnet_name      = "SUB-${var.dr_prefix}-RDS"
 dr_data_subnet_az1_name = "SUB-${var.dr_prefix}-DATA-AZ1"
 dr_data_subnet_az2_name = "SUB-${var.dr_prefix}-DATA-AZ2"
 dr_data_subnet_az3_name = "SUB-${var.dr_prefix}-DATA-AZ3"
 environment             = "${var.dr}"
 tag_name                = ""
 
 providers =  {
  aws = "aws.dr"
 }
 
}
*/


###EFS

module "efs" {
 source                = "./efs"
 creation-token        = "EFS-${var.prefix}"
 name                  = "EFS-${var.prefix}"
 performance-mode      = "generalPurpose"
 throughput-mode       = "bursting"  
 security_group        = ["${module.sg.efs_sg}"]
 front_app_subnet_1    = "${module.vpc.front_az1_subnet}"
 front_app_subnet_2    = "${module.vpc.front_az2_subnet}"
 front_app_subnet_3    = "${module.vpc.front_az3_subnet}"
 prefix                 = "${var.prefix}"
 kms_key               = "${module.kms.kms_key_efs}"
}


###ENDPOINT

module "endpoint"{
 source                    = "./endpoint"
 vpc_id                    = "${module.vpc.vpc_id}"
 service_s3_name           = "com.amazonaws.${var.region}.s3"
 service_ec2_name          = "com.amazonaws.${var.region}.ec2"
 service_kms_name          = "com.amazonaws.${var.region}.kms"
 ec2_sg                    = ["${module.sg.ec2_endpoint_sg}"]
 kms_sg                    = ["${module.sg.kms_endpoint_sg}"]
 s3_endpoint_rttbl_assoc   = ["${module.vpc.az1_rt_tbl}","${module.vpc.az2_rt_tbl}","${module.vpc.az3_rt_tbl}"]
 ec2_name                  = "END-${var.prefix}-EC2"
 s3_name                   = "END-${var.prefix}-S3"
 kms_name                  = "END-${var.prefix}-KMS"
 environment               = "${var.environment}"
}

###LOAD BALANCER

module "lb" {
 source                  = "./elb"
 external_alb_name       = "ALB-${var.prefix}-EXTERNAL"
 vpc_id                  = "${module.vpc.vpc_id}"
 az_subnet_id_az1        = "${module.vpc.data_subnet_az1}"
 az_subnet_id_az2        = "${module.vpc.data_subnet_az2}"
 az_subnet_id_az3        = "${module.vpc.data_subnet_az3}"
 sec_grp                 = ["${module.sg.alb_sg}"]
 subnet                  = ["${module.vpc.public_subnet}"]
 environment             = "${var.environment}"
}




###CLOUDTRAIL

module "cloudtrail" {
  source                        = "./cloudtrail"
  stage                         = "${var.environment}"
  name                          = "CLOUDTRAIL"
  enable_log_file_validation    = true
  include_global_service_events = true
  is_multi_region_trail         = false
  enable_logging                = true
  s3_bucket_name                = "${module.cloudtrail_s3_bucket.bucket_id}"
}

module "cloudtrail_s3_bucket" {
  source    = "./s3_cloudtrail"
  namespace = "S3-AU"
  stage     = "${var.environment}"
  name      = "cloudtrail"
  region    = "${var.region}"
  ###bucket name convention = "namespace-stage-name"
}

/*

###EKS

module "eks" {
  source         = "./eks"
  cluster_name   = "EKS-${var.prefix}"
  subnets        = ["${module.vpc.data_subnet_az1}","${module.vpc.data_subnet_az2}"]
  secgrp_name    = "SEC-${var.prefix}-EKS"
  worker_prefix  = "${var.prefix}"
  tags = {
    environment  = "${var.environment}"
  }

  vpc_id             = "${module.vpc.vpc_id}"
  worker_group_count = 1

  worker_groups = [

    {
      name                          = "WORKER-GRP-LARGE"
      instance_type                 = "c5.large"
      additional_userdata           = ""
      #additional_security_group_ids = "${module.sg.worker_group_mgmt_two_id}"
      additional_security_group_ids = ""
      asg_desired_capacity          = 0
      asg_max_size                  = 0
      asg_min_size                  = 0
      key_name                      = "${var.ssh_kp}"
      spot_price                    = "0.1"
    },
    {
      name                          = "WORKER-GRP-XLARGE"
      instance_type                 = "c5.xlarge"
      additional_userdata           = ""
      #additional_security_group_ids = "${module.sg.worker_group_mgmt_two_id}"
      additional_security_group_ids = ""
      asg_desired_capacity          = 4
      asg_max_size                  = 4
      asg_min_size                  = 4
      key_name                      = "${var.ssh_kp}"
    }
  ]

  worker_additional_security_group_ids = ["${module.sg.all_worker_mgmt_id}"]
  workers_additional_policies_count    = 1
  workers_additional_policies          = ["${data.aws_iam_policy.ecr_rw.arn}"]
  map_roles                            = "${var.map_roles}"
  map_roles_count                      = "${var.map_roles_count}"
  map_users                            = "${var.map_users}"
  map_users_count                      = "${var.map_users_count}"
  map_accounts                         = "${var.map_accounts}"
  map_accounts_count                   = "${var.map_accounts_count}"
}
*/
