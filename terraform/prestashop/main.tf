data "aws_ami" "ami_prestashop" {

 filter {
  
 
  name     = "image-id"
  values   = ["${var.ami}"]
 }
 owners = ["784571545063"]
 
}


data "aws_subnet" "front_subnet" {

 count = "${length(var.front_subnet)}"
 id    = "${element(var.front_subnet,count.index)}"

}

data "aws_acm_certificate" "example" {
  domain   = "rest-solution.net"
  statuses = ["ISSUED"]
}

data "aws_acm_certificate" "decathlon" {
  domain   = "*.decathlon.sg"
  statuses = ["ISSUED"]
}



###Auto Scaling

resource "aws_autoscaling_group" "prestashop" {
 #name                 = "${aws_launch_configuration.prestashop.name}"
 name                 = "${var.asg_name}"
 max_size             = "${var.asg_max_size}"
 min_size             = "${var.asg_min_size}"
 desired_capacity     = "${var.asg_desired_capacity}"
 vpc_zone_identifier  = ["${var.subnet}"]
 target_group_arns    = ["${aws_lb_target_group.prestashop_group_80.id}","${aws_lb_target_group.prestashop_group_443.id}"]
 health_check_type    = "EC2"
 launch_configuration = "${aws_launch_configuration.prestashop.name}"
 
 lifecycle {
    create_before_destroy = true
    #ignore_changes        = ["desired_capacity"]
  }

 tags = ["${concat(
  list(
   map("key","Name","value","${var.name}-AZ","propagate_at_launch",true),
   map("key","environment","value","${var.environment}","propagate_at_launch",true)
  )
 )}"]
 
}

resource "aws_launch_configuration" "prestashop" {
 name_prefix   = "${var.name}"
 image_id      = "${data.aws_ami.ami_prestashop.id}"
 instance_type = "${var.instance_type}"
 security_groups = ["${var.sg}"]
 key_name      = "${var.key_name}"
 user_data     = "${data.template_file.efs_mount.rendered}"
 enable_monitoring = true
 lifecycle {
    create_before_destroy = true
  }

}


###Auto Saling Policy

resource "aws_autoscaling_policy" "scale-up" {
  name      		     = "AUTOSCALING-POLICY-PRESTASHOP-UP"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = "${aws_autoscaling_group.prestashop.name}"
  
  lifecycle {
   create_before_destroy = true
 }

}

resource "aws_autoscaling_policy" "scale-down" {
  name      		     = "AUTOSCALING-POLICY-PRESTASHOP-DOWN"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = "${aws_autoscaling_group.prestashop.name}"
  
 lifecycle {
   create_before_destroy = true
  }
}


###Cloudwatch Metric Alarm

resource "aws_cloudwatch_metric_alarm" "cpu-high" {
  alarm_name = "CLOUDWATCH-ALARM-PRESTASHOP-CPU-HIGH"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "60"
  evaluation_periods = "1"
  statistic = "Average"
  threshold = "60"
  alarm_description = "This metric monitors ec2 cpu for high utilization on prestashop instance"
  alarm_actions = [
    "${aws_autoscaling_policy.scale-up.arn}"
  ]
  ok_actions    = ["${aws_autoscaling_policy.scale-down.arn}"]
  
  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.prestashop.name}"
  }
}


/*

resource "aws_cloudwatch_metric_alarm" "cpu-low" {
  alarm_name = "CLOUDWATCH-ALARM-PRESTASHOP-CPU-LOW"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "900"
  statistic = "Average"
  threshold = "5"
  alarm_description = "This metric monitors ec2 cpu for low utilization on prestashop instance"
  alarm_actions = [
    "${aws_autoscaling_policy.scale-down.arn}"
  ]
  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.prestashop.name}"
  }
}




resource "aws_instance" "ec2_prestashop" {
 ami                       = "${data.aws_ami.ami_prestashop.id}"
 count                     = "${var.instance_count}"
 instance_type             = "${var.instance_type}"
 key_name                  = "${var.key_name}"
 vpc_security_group_ids    = ["${var.sg}"]
 private_ip                = "${cidrhost(element(data.aws_subnet.front_subnet.*.cidr_block,count.index),11)}"
 subnet_id                 = "${element(var.subnet,count.index)}"
 monitoring                = true
 user_data                 = "${data.template_file.efs_mount.rendered}"
    
 tags {
  Name                     = "${var.name}-AZ${count.index+1}"
  Environment              = "${var.environment}"
 }

}
*/

##Template file

data "template_file" "efs_mount" {
 template = "${file("${path.module}/efs_mount.sh.tpl")}"

 vars {
       efs_id      = "${var.efs_id}"
	   db_host     = "${var.db_host}"
       db_user     = "${var.db_user}"
       db_password = "${var.db_password}"
 }
}



###LB TARGET GROUP

resource "aws_lb_target_group" "prestashop_group_80" {
 name        = "${var.prestashop_tg_name}-80"
 port        = "80"
 protocol    = "HTTP"
 target_type = "instance"
 vpc_id      = "${var.vpc_id}"
 stickiness {
  type   = "lb_cookie"
  cookie_duration = "3600"
 }
 health_check {
  matcher = "200,300-399"
 }

}

resource "aws_lb_target_group" "prestashop_group_443" {
 name        = "${var.prestashop_tg_name}-443"
 port        = "443"
 protocol    = "HTTPS"
 target_type = "instance"
 health_check {
  matcher = "200,300-399"
 }
 stickiness {
  type   = "lb_cookie"
  cookie_duration = "3600"
 }
 
 vpc_id      = "${var.vpc_id}"

}

###LB LISTENER


resource "aws_lb_listener" "prestashop_alb_listener_80" {

 load_balancer_arn  = "${var.external_alb}"
 protocol           = "HTTP"
 port               = "80"
 default_action  {
  type             = "forward"
  target_group_arn = "${aws_lb_target_group.prestashop_group_80.arn}"
 }
}

/*

resource "aws_lb_listener" "prestashop_alb_listener_443" {

 load_balancer_arn  = "${var.external_alb}"
 protocol           = "HTTPS"
 port               = "443"
 ssl_policy         = "ELBSecurityPolicy-2016-08"
 certificate_arn    = "${data.aws_acm_certificate.decathlon.arn}"
 default_action  {
  type             = "forward"
  target_group_arn = "${aws_lb_target_group.prestashop_group_443.arn}"
 }
}



###LB TARGET GROUP ATTACHMENT

resource "aws_lb_target_group_attachment" "prestashop_alb_attach_80" {

 count            = "${var.instance_count}"
 target_group_arn = "${aws_lb_target_group.prestashop_group_80.arn}"
 #target_id        = "${element(aws_instance.ec2_prestashop.*.id,count.index)}"
 target_id        = "${element()}"
 port             = "80"
 
 lifecycle {
  ignore_changes = ["id","target_id"] 
 }	
 
}


resource "aws_lb_target_group_attachment" "prestashop_alb_attach_443" {

 count            = "${var.instance_count}"
 target_group_arn = "${aws_lb_target_group.prestashop_group_443.arn}"
 target_id        = "${element(aws_instance.ec2_prestashop.*.id,count.index)}"
 port             = "443"
}



###LISTENER CERTIFICATE 

resource "aws_lb_listener_certificate" "example" {
  listener_arn    = "${aws_lb_listener.prestashop_alb_listener_443.arn}"
  certificate_arn = "${var.certificate}"
}
*/
