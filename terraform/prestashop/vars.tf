variable "ami_name" {

}

variable "instance_type" {

}

variable "key_name" {

}

variable "sg" {
 default = []
}

variable "subnet" {
 default = []
}

variable "name" {

}

variable "environment" {

}

variable "instance_count" {

}

variable "vpc_id" {

}

variable "prestashop_tg_name" {

}

variable "external_alb" {

}

variable "db_host" {
 
}

variable "db_user" {
 
}

variable "db_password" {
 
}

variable "efs_id" {

} 

variable "front_subnet" {
 default = []
}

variable "asg_max_size" {

}

variable "asg_min_size" {

}

variable "asg_desired_capacity" {

}

variable "asg_name" {

}

variable "ami" {

}
