variable "creation-token" {
 description = "unique name used as reference when creating the efs"
}

variable "performance-mode"{
 description = "The file system performance mode. Can be either generalPurpose or maxIO"
}

variable "throughput-mode"{
 description =  "Throughput mode for the file system. Defaults to bursting. Valid values: bursting, provisioned." 
}

variable "front_app_subnet_1"{
 description = "Front App Subnet"
 
}

variable "front_app_subnet_2"{
 description = "Front App Subnet"
 
}

variable "front_app_subnet_3"{
 description = "Front App Subnet"
 
}

variable "security_group"{
 description = ""
 default     = []
}


variable "kms_key" {
 description  =  "KMS key for EFS encryption"
}

variable "name" {
 description  = "Name of EFS"
}

variable "prefix" {

}