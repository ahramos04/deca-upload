output "db_host" {
 value = "${aws_rds_cluster.default.endpoint}"
}

output "username" {
  value = "${aws_rds_cluster.default.master_username}"
}


output "rds_arn" {
     value = "${aws_rds_cluster.default.arn}"
}
