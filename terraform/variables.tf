

variable "profile" {
  default     = "deca"
}

variable "account" {
  default     = "784571545063"
}

variable "prestashop_ami" {
  default    = "ami-0586f9b361c3ad761"
}

### ENVIRONMENT#

variable "production" {
  description = "Production environment tag"
  default     = "PRODUCTION"
}

variable "sit" {
  description = "SIT environment tag"
  default     = "SIT"

}

variable "dr" {
  description = "DR environment tag"
  default     = "DR"
}

variable "uat" {
  description = "UAT environment tag"
  default     = "UAT"
}

variable "DEVOPS" {
  description = "Devops environment tag"
  default     = "DEVOPS"
}

variable "preprod" {
  description = "Preprod environment tag"
  default     = "PREPROD"
}


###EKS


variable "map_accounts" {
  description = "Additional AWS account numbers to add to the aws-auth configmap."
  type        = "list"

  default = [
    "777777777777",
    "888888888888",
  ]
}

variable "map_accounts_count" {
  description = "The count of accounts in the map_accounts list."
  type        = "string"
  default     = 0
}

variable "map_roles" {
  description = "Additional IAM roles to add to the aws-auth configmap."
  type        = "list"

  default = [
    {
      role_arn = "arn:aws:iam::66666666666:role/role1"
      username = "role1"
      group    = "system:masters"
    },
  ]
}

variable "map_roles_count" {
  description = "The count of roles in the map_roles list."
  type        = "string"
  default     = 0
}

variable "map_users" {
  description = "Additional IAM users to add to the aws-auth configmap."
  type        = "list"

  default = [   
    {
      user_arn = "arn:aws:iam::66666666666:user/user2"
      username = "user2"
      group    = "system:masters"
    },
  ]
}

variable "map_users_count" {
  description = "The count of roles in the map_users list."
  type        = "string"
  default     = 1
}

