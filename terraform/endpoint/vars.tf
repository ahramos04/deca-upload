variable "environment"{
 default = "Production"
}

variable "vpc_id" {
 description = "VPC ID"
}

variable "service_s3_name"{
 description = "s3 endpoint Name"
}

variable "service_ec2_name"{
 description = "ec2 endpoint Name"
}

variable "service_kms_name"{
 description = "kms endpoint Name"
}


/*
variable "service_sns_name"{
 description = "sns endpoint Name"
}
*/

variable "s3_endpoint_rttbl_assoc"{
 description = "AWS endpoint route table association"
 default = []
}

variable "ec2_endpoint_rttbl_assoc"{
 description = "AWS endpoint route table association"
 default = []
}
variable "kms_endpoint_rttbl_assoc"{
 description = "AWS endpoint route table association"
 default = []
}
variable "sns_endpoint_rttbl_assoc"{
 description = "AWS endpoint route table association"
 default = []
}

variable "ec2_sg"{
 default  = []
 
}

/*
variable "sns_sg"{
 default  = []
 
}
*/

variable "kms_sg"{
 default  = []
 
}


variable "s3_name" {

}

variable "ec2_name" {

}

variable "kms_name" {

}
/*
variable "sns_name" {

}
*/
