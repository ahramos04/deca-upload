#Install Git and Net-Tools
sudo yum install net-tools -y
sudo yum install git -y

#Install Apache HTTPD
sudo yum install httpd mod_ssl openssl -y
sudo systemctl enable httpd 
sudo systemctl start httpd
sudo cp  /etc/httpd/conf/httpd.conf  /etc/httpd/conf/httpd.conf.bak
#sudo sed -i '/<Directory "\/var\/www\/html">/,/<\/Directory>/ s/AllowOverride None/AllowOverride All/' /etc/httpd/conf/httpd.conf
sudo sed -i 's/DocumentRoot "\/var\/www\/html"/DocumentRoot "\/var\/www\/html\/decasg"/' /etc/httpd/conf/httpd.conf

#Install Amazon EFS Utils
sudo yum install amazon-efs-utils -y

#Install Memcached
sudo yum install memcached -y
sudo systemctl enable memcached
sudo systemctl start memcached

#Add PHP extension
sudo sed -i 's/DirectoryIndex index\.html/DirectoryIndex index\.html \*\.php/' /etc/httpd/conf/httpd.conf
sudo systemctl restart httpd

#install php 7.2
sudo amazon-linux-extras install epel -y
sudo yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y
sudo yum install yum-utils
sudo yum-config-manager --enable remi-php72
sudo yum update -y
sudo yum install php72-php -y
sudo yum install php72-php-fpm php72-php-gd php72-php-json php72-php-mbstring php72-php-mysqlnd php72-php-xml php72-php-xmlrpc php72-php-opcache php72-php-pecl-zip php72-php-intl php72-php-pecl-memcache -y
sudo systemctl enable php72-php-fpm.service
sudo systemctl start php72-php-fpm.service

#Install rsync
sudo yum install rsync -y


#Install COMPOSER
sudo php72 -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
HASH="$(wget -q -O - https://composer.github.io/installer.sig)"
sudo php72 -r "if (hash_file('SHA384', 'composer-setup.php') === '$HASH') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
sudo php72 composer-setup.php --install-dir=/usr/local/bin --filename=composer

#Clone Prestashop 1.7.4.x branch
git clone https://github.com/PrestaShop/PrestaShop.git -b 1.7.4.x

#Copy PrestaShop to /var/ww/html/
sudo mv /home/ec2-user/PrestaShop/admin-dev /home/ec2-user/PrestaShop/presta_admin && \
sudo cp -rf PrestaShop /var/www/html/decasg && \
sudo cp -rf /home/ec2-user/decashopsg/* /var/www/html/decasg


#Create logs and certs directory

sudo mkdir /var/www/logs
#sudo mkdir /var/www/certs

#Copy certs to /var/www/certs

sudo cp /home/ec2-user/*.sg.* /var/www/certs
sudo chmod 644 /var/www/certs/* && sudo chown root: /var/www/certs/*

#Copy httpd and ssl conf
sudo cp /home/ec2-user/httpd.conf /etc/httpd/conf/httpd.conf
sudo chown root:root /etc/httpd/conf/httpd.conf
sudo chmod 644 /etc/httpd/conf/httpd.conf

sudo cp /home/ec2-user/ssl.conf /etc/httpd/conf.d/ssl.conf
sudo chown root:root /etc/httpd/conf.d/ssl.conf
sudo chmod 644 /etc/httpd/conf.d/ssl.conf 
 
#Modify PrestaShop directory
sudo chown apache:apache -R /var/www/html/decasg/


