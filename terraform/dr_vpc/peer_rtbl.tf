
data "aws_route_table" "peer_az1" {

 filter {
  
  name   = "tag:Name" 
  values = ["${var.peer_az1_rtbl}"]
  
 }
}

data "aws_route_table" "peer_az2" {
 
 filter {
  
  name   = "tag:Name" 
  values = ["${var.peer_az2_rtbl}"]
 
 }
}

data "aws_route_table" "peer_az3" {

 filter {
  
  name   = "tag:Name" 
  values = ["${var.peer_az3_rtbl}"]
 
 }
}

data "aws_vpc_peering_connection" "dr" {
 filter {
 
  name = "tag:Name"
  values = ["${var.dr_vpc_peering_name}"]
 
 }
}


resource "aws_route" "route_az1" {
  route_table_id            = "${data.aws_route_table.peer_az1.id}"
  destination_cidr_block    = "${var.vpc_subnet}"
  vpc_peering_connection_id = "${data.aws_vpc_peering_connection.dr.id}"
}

resource "aws_route" "route_az2" {
  route_table_id            = "${data.aws_route_table.peer_az2.id}"
  destination_cidr_block    = "${var.vpc_subnet}"
  vpc_peering_connection_id = "${data.aws_vpc_peering_connection.dr.id}"
}

resource "aws_route" "route_az3" {
  route_table_id            = "${data.aws_route_table.peer_az3.id}"
  destination_cidr_block    = "${var.vpc_subnet}"
  vpc_peering_connection_id = "${data.aws_vpc_peering_connection.dr.id}"
}

