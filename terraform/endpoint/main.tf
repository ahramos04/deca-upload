resource "aws_vpc_endpoint" "s3" {
 vpc_id    = "${var.vpc_id}"
 service_name = "${var.service_s3_name}"

 tags {
  Name = "${var.s3_name}"
  environment = "${var.environment}"
 }
}


resource "aws_vpc_endpoint" "ec2" {
 vpc_id            = "${var.vpc_id}"
 service_name      = "${var.service_ec2_name}" 
 vpc_endpoint_type = "Interface"
 security_group_ids = ["${var.ec2_sg}"]

  tags {
  Name = "${var.ec2_name}"
  environment = "${var.environment}"
 }

}

resource "aws_vpc_endpoint" "kms" {
 vpc_id             = "${var.vpc_id}"
 service_name       = "${var.service_kms_name}"
 vpc_endpoint_type  = "Interface"
 security_group_ids = ["${var.kms_sg}"] 

  tags {
  Name = "${var.kms_name}"
  environment = "${var.environment}"
 }

}

/*
resource "aws_vpc_endpoint" "sns" {
 vpc_id             = "${var.vpc_id}"
 service_name       = "${var.service_kms_name}"
 vpc_endpoint_type  = "Interface"
 security_group_ids = ["${var.sns_sg}"] 

  tags {
  Name = "${var.sns_name}"
  environment = "${var.environment}"
 }

}
*/


resource "aws_vpc_endpoint_route_table_association" "s3_endpoint" {
  #count           = "${length(var.s3_endpoint_rttbl_assoc)}"
  count           = 3
  route_table_id  = "${element(var.s3_endpoint_rttbl_assoc,count.index)}"
  vpc_endpoint_id = "${aws_vpc_endpoint.s3.id}"
}



