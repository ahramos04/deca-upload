/*
resource "aws_autoscaling_policy" "up" {
  name                   = "foobar3-terraform-test"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = "${aws_autoscaling_group.workers.*.name[1]}"
}
*/


/*

resource "aws_autoscaling_policy" "scale-up" {
  name      		 = "${format("%s-AUTOSCALING-POLICY-WORKER-UP",var.cluster_name)}"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
#  autoscaling_group_name = "${aws_autoscaling_group.workers.name}"
   autoscaling_group_name = "${aws_autoscaling_group.workers.name}"
}

resource "aws_autoscaling_policy" "scale-down" {
  name      		 = "${format("%s-AUTOSCALING-POLICY-WORKER-DOWN",var.cluster_name)}"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = "${aws_autoscaling_group.workers.name}"
}

resource "aws_cloudwatch_metric_alarm" "cpu-high" {
  alarm_name = "${format("%s-CLOUDWATCH-ALARM-WORKER-CPU-HIGH",var.cluster_name)}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "300"
  statistic = "Average"
  threshold = "100"
  alarm_description = "This metric monitors ec2 cpu for high utilization on worker hosts"
  alarm_actions = ["${aws_autoscaling_policy.scale-up.arn}"]
  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.workers.name}"
  }
}

resource "aws_cloudwatch_metric_alarm" "cpu-low" {
  alarm_name = "${format("%s-CLOUDWATCH-ALARM-WORKER-CPU-LOW",var.cluster_name)}"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "900"
  statistic = "Average"
  threshold = "5"
  alarm_description = "This metric monitors ec2 cpu for low utilization on worker hosts"
  alarm_actions = ["${aws_autoscaling_policy.scale-down.arn}"]
  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.workers.name}"
  }
}
*/
