#!/bin/bash

git pull
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR
cd packer
OLDAMIID=$(grep ami- packer_ami.json | sed -e "s/\"//g" -e "s/,//" | awk -F':' '{print $2}' | sed "s/ //")
AMIID=$(cat packer-manifest.json | jq .builds[].artifact_id | tail -1 | sed "s/\"//g" | awk -F':' '{print $2}')
if [ $OLDAMIID == $AMIID ]; then
	echo "Base image did not change"
else
	echo "Using $AMIID replacing $OLDAMIID"
fi
sed -i "s/$OLDAMIID/$AMIID/" packer_ami.json
sudo /usr/local/bin/packer build packer_ami.json
AMIID=$(cat packer-manifest.json | jq .builds[].artifact_id | tail -1 | sed "s/\"//g" | awk -F':' '{print $2}')
cd ../terraform
OLDAMIID=$(grep -A 1 prestashop_ami variables.tf | tail -1 | sed "s/\"//g" | awk -F'=' '{print $2}' | sed "s/ //")
echo "Using $AMIID replacing $OLDAMIID"
sed -i "s/$OLDAMIID/$AMIID/" variables.tf
terraform apply -target=module.prestashop -auto-approve 
git commit -a -m "automatic git build updating $OLDAMIID to $AMIID"
git push origin

