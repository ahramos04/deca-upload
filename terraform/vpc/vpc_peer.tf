### VPC PEERING TO DR

provider "aws" {
  alias  = "dr"
  region = "${var.peer_region}"

  # Accepter's credentials.
}

data "aws_caller_identity" "peer" {
  provider = "aws.dr"
}


resource "aws_vpc_peering_connection" "foo" {
  peer_vpc_id   = "${var.peer_vpc_id}"
  vpc_id        = "${aws_vpc.vpc.id}"
  peer_region   = "${var.peer_region}"

  tags {
   Name = "VPC-PEER-PREPROD-TO-DR"
  }
}


resource "aws_vpc_peering_connection_accepter" "peer" {
  provider                  = "aws.dr"
  vpc_peering_connection_id  = "${aws_vpc_peering_connection.foo.id}"
  auto_accept = true
  tags = {
    Name = "VPC-PEER-PREPROD-TO-DR"
    Side = "Accepter"
  }
}
