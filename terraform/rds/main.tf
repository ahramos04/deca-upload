###ROUTE 53 DATA SOURCE

data "aws_route53_zone" "decathlon_local"{
 name          = "decathlon.local."
 private_zone  = true
}


### RDS SUBNET

resource "aws_db_subnet_group" "db_subnet_grp"{
 name = "${lower(var.rds_subnet_name)}"
 subnet_ids = ["${var.rds_subnet_group}"]
 tags {
  name = "${var.rds_subnet_name}"
  environment = "${var.environment}"
 }
}


###RDS CLUSTER PARAMETER

resource "aws_rds_cluster_parameter_group" "default" {
  name         = "rds-${lower(var.prefix)}-cluster-pg"
  family       = "aurora5.6"
  description  = "RDS custom cluster parameter group"
 
 parameter {
    name  = "binlog_format"
    value = "MIXED"
	apply_method = "pending-reboot"
  }

}


### RDS CLUSTER

resource "aws_rds_cluster" "default" {
  cluster_identifier              = "${lower(var.prefix)}-${lower(var.db_identifier)}-cluster"
  #availability_zones              = ["${var.availability_zones}"]
  database_name                   = "${var.db_identifier}"
  master_username                 = "${var.db_username}"
  master_password                 = "${var.db_password}"
  engine                          = "${var.engine}"
  engine_version                  = "${var.engine_version}"
  engine_mode                     = "serverless"
  vpc_security_group_ids          = ["${var.rds_sg}"]
  storage_encrypted               = true
  db_subnet_group_name            = "${aws_db_subnet_group.db_subnet_grp.id}"
  kms_key_id                      = "${var.kms_key}"
  db_cluster_parameter_group_name = "${aws_rds_cluster_parameter_group.default.id}"
  skip_final_snapshot             = "${var.skip_snapshot}" 
  
  tags{
    Environment = "${var.environment}"
  }

  scaling_configuration {
    auto_pause               = false
    max_capacity             = 8
    min_capacity             = 2
    #seconds_until_auto_pause = 300
    timeout_action           = "ForceApplyCapacityChange"
  }

  lifecycle {
   ignore_changes         = ["kms_key_id","availability_zones"]
 }

}

###ROUTE 53 

resource "aws_route53_record" "rds-sg-local" {
  zone_id = "${data.aws_route53_zone.decathlon_local.zone_id}"
  name    = "${var.prefix}-${aws_rds_cluster.default.database_name}"
  type    = "CNAME"
  ttl     = "300"
  #records = ["${var.rds_endpoint[count.index]}"]
  records = ["${aws_rds_cluster.default.*.endpoint}"]
}


/*
### RDS CLUSTER 0 INSTANCE


resource "aws_rds_cluster_instance" "cluster_instances_0" {
  count                           = "2"
  identifier                      = "${lower(var.prefix)}-${var.db_identifier}-${count.index+1}"
  cluster_identifier              = "${aws_rds_cluster.default.id}"
  instance_class                  = "${var.instance_class}"
  engine                          = "${aws_rds_cluster.default.engine}"
  engine_version                  = "${aws_rds_cluster.default.engine_version}"
  db_subnet_group_name            = "${aws_db_subnet_group.db_subnet_grp.id}"
  promotion_tier                  = "0"
  #performance_insights_enabled    = true
  #performance_insights_kms_key_id = "${var.kms_key}"  

  tags{
    Environment = "${var.environment}"
  }

  lifecycle {
 
  ignore_changes                 = ["performance_insights_kms_key_id"]
   
  }
  
}

*/
