
##VPC

resource "aws_vpc" "vpc"{
 
 cidr_block           = "${var.vpc_subnet}"
 enable_dns_support   = true
 enable_dns_hostnames = true
 tags = {
  Name                = "${var.vpc_name}"  
  Environment         = "${var.environment}"
 }
}

###INTERNET GATEWAY

resource "aws_internet_gateway" "igw"{

 vpc_id               = "${aws_vpc.vpc.id}"
 tags {
   Name               = "${var.igw_name}"
   Environment        = "${var.environment}"
 }
}

###SUBNETS 


resource "aws_subnet" "public_subnet"{
  vpc_id                  = "${aws_vpc.vpc.id}"
  count                   = "${length(var.public_subnet)}"
  availability_zone       = "${element(data.aws_availability_zones.aws_zones.names,count.index)}"
  cidr_block              = "${element(var.public_subnet,count.index)}"
  map_public_ip_on_launch = true
  tags = "${
   map(
   "Name","${element(var.public_subnet_names,count.index)}",
   "environment","${var.environment}",
   "kubernetes.io/cluster/${var.eks_cluster_name}","shared",
   "kubernetes.io/role/internal-elb","1",
   "kubernetes.io/role/alb-ingress","1",
   )
  }"
  
}


resource "aws_subnet" "az1_subnet"{
  vpc_id                  = "${aws_vpc.vpc.id}"
  count                   = "${length(var.az1_subnet)}"
  availability_zone       = "${var.az1_subnet_az}"
  cidr_block              = "${element(var.az1_subnet,count.index)}"
  tags = "${
   map(
   "Name","${element(var.az1_subnet_names,count.index)}",
   "environment","${var.environment}",
   "kubernetes.io/cluster/${var.eks_cluster_name}","shared",
   "kubernetes.io/role/internal-elb","1",
   "kubernetes.io/role/alb-ingress","1",
   )
  }"
}

resource "aws_subnet" "az2_subnet"{
  vpc_id                  = "${aws_vpc.vpc.id}"
  count                   = "${length(var.az2_subnet)}"
  availability_zone       = "${var.az2_subnet_az}" 
  cidr_block              = "${element(var.az2_subnet,count.index)}"
  tags = "${
   map(
   "Name","${element(var.az2_subnet_names,count.index)}",
   "environment","${var.environment}",
   "kubernetes.io/cluster/${var.eks_cluster_name}","shared",
   "kubernetes.io/role/internal-elb","1",
   "kubernetes.io/role/alb-ingress","1",
   )
  }"
}

resource "aws_subnet" "az3_subnet"{
  vpc_id                  = "${aws_vpc.vpc.id}"
  count                   = "${length(var.az3_subnet)}"
  availability_zone       = "${var.az3_subnet_az}" 
  cidr_block              = "${element(var.az3_subnet,count.index)}"
  tags = "${
   map(
   "Name","${element(var.az3_subnet_names,count.index)}",
   "environment","${var.environment}",
   "kubernetes.io/cluster/${var.eks_cluster_name}","shared",
   "kubernetes.io/role/internal-elb","1",
   "kubernetes.io/role/alb-ingress","1",
   )
  }"
}



resource "aws_subnet" "weave_subnet"{
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${var.weave_cidr}"
  tags {
   Name                   = "${var.weave_name}"
   environment            = "${var.environment}"
  }
}

###ROUTE TABLE

resource "aws_route_table" "public_route" {

 vpc_id             = "${aws_vpc.vpc.id}"

 route {
  cidr_block        = "0.0.0.0/0"
  gateway_id        = "${aws_internet_gateway.igw.id}"
 }

 route {
  cidr_block        = "${var.dr_cidr_block}"
  gateway_id        = "${aws_vpc_peering_connection.foo.id}"
 }
  
 tags{
  Name              = "${var.public_rt_table_name}"
  Environment       = "${var.environment}"
 }
}


resource "aws_route_table" "az1_route" {

 vpc_id             = "${aws_vpc.vpc.id}"

 route {
  cidr_block        = "0.0.0.0/0"
  nat_gateway_id      = "${aws_nat_gateway.natgw_az1.id}"
 }

 route {
  cidr_block        = "${var.dr_cidr_block}"
  gateway_id        = "${aws_vpc_peering_connection.foo.id}"
 }

 tags{
  Name              = "${var.az1_rt_table_name}"
  Environment       = "${var.environment}"
 }
}

resource "aws_route_table" "az2_route" {

 vpc_id             = "${aws_vpc.vpc.id}"

 route {
  cidr_block        = "0.0.0.0/0"
  nat_gateway_id      = "${aws_nat_gateway.natgw_az2.id}"
 }
 
 route {
  cidr_block        = "${var.dr_cidr_block}"
  gateway_id        = "${aws_vpc_peering_connection.foo.id}"
 }
 
 tags{
  Name              = "${var.az2_rt_table_name}"
  Environment       = "${var.environment}"
 }
}

resource "aws_route_table" "az3_route" {

 vpc_id             = "${aws_vpc.vpc.id}"

 route {
  cidr_block        = "0.0.0.0/0"
  nat_gateway_id      = "${aws_nat_gateway.natgw_az3.id}"
 }
 
 route {
  cidr_block        = "${var.dr_cidr_block}"
  gateway_id        = "${aws_vpc_peering_connection.foo.id}"
 } 
 
 tags{
  Name              = "${var.az3_rt_table_name}"
  Environment       = "${var.environment}"
 }
}



###ROUTE ASSOCIATION

resource "aws_route_table_association" "public_assocation_rt"{
 count          = "${length(var.public_subnet)}"
 subnet_id      = "${element(aws_subnet.public_subnet.*.id,count.index)}"
 route_table_id = "${aws_route_table.public_route.id}"
}

resource "aws_route_table_association" "az1_assocation_rt"{
 count          = "${length(var.az1_subnet)}"
 subnet_id      = "${element(aws_subnet.az1_subnet.*.id,count.index)}"
 route_table_id = "${aws_route_table.az1_route.id}"
}

resource "aws_route_table_association" "az2_assocation_rt"{
 count          = "${length(var.az2_subnet)}"
 subnet_id      = "${element(aws_subnet.az2_subnet.*.id,count.index)}"
 route_table_id = "${aws_route_table.az2_route.id}"
}

resource "aws_route_table_association" "az3_assocation_rt"{
 count          = "${length(var.az3_subnet)}"
 subnet_id      = "${element(aws_subnet.az3_subnet.*.id,count.index)}"
 route_table_id = "${aws_route_table.az3_route.id}"
}

###ELASTIC IP

resource "aws_eip" "eip_az1"{
 vpc = true
 tags {
  Name = "${var.eip_az1_name}"
  environment = "${var.environment}"
 }
}


resource "aws_eip" "eip_az2"{
 vpc = true
 tags {
  Name = "${var.eip_az2_name}"
  environment = "${var.environment}"
 }
}

resource "aws_eip" "eip_az3"{
 vpc = true
 tags {
  Name = "${var.eip_az3_name}"
  environment = "${var.environment}"
 }
}

###NAT GATEWAY

resource "aws_nat_gateway" "natgw_az1"{
  allocation_id = "${aws_eip.eip_az1.id}"
  subnet_id     = "${aws_subnet.public_subnet.*.id[0]}"
  depends_on    = ["aws_internet_gateway.igw"]
  tags{
   Name         = "${var.az1_natgw_name}"
   Environment  = "${var.environment}"
  }
}

resource "aws_nat_gateway" "natgw_az2"{
  allocation_id = "${aws_eip.eip_az2.id}"
  subnet_id     = "${aws_subnet.public_subnet.*.id[1]}"
  depends_on    = ["aws_internet_gateway.igw"]
  tags{
   Name         = "${var.az2_natgw_name}"
   Environment  = "${var.environment}"
  }
}

resource "aws_nat_gateway" "natgw_az3"{
  allocation_id = "${aws_eip.eip_az3.id}"
  subnet_id     = "${aws_subnet.public_subnet.*.id[2]}"
  depends_on    = ["aws_internet_gateway.igw"]
  tags{
   Name         = "${var.az3_natgw_name}"
   Environment  = "${var.environment}"
  }
}

