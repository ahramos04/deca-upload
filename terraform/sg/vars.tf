variable "vpc_id" {

}

variable "environment" {

} 

variable "rfid_sg_name" {
 default = ""
}

variable "th_sg_name" {
 default = ""
}

variable "elab_prod_name" {
 default = ""
}

variable "lbn_mgt_name" {
 default = ""
}

variable "decasg_preprod_name" {
 default = ""
}

variable "decashop_elastic_name" {
 default = ""
}

variable "bastion_sg_name" {

}

variable "rds_sg_name" {


}

variable "efs_sg_name" {

}

variable "eks_worker_mgmt_one_name" {

}

variable "eks_worker_mgmt_two_name" {

}

variable "eks_all_worker_mgmt_name" {

}

variable "eks_cidr" {

}

variable "kms_endpoint_name" {

}

variable "ec2_endpoint_name" {

}

variable "alb_sg_name" {

}

variable "prestashop_sg_name" {

}

variable "datasync_sg_name" {

}