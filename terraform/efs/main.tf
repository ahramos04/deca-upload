data "aws_route53_zone" "decathlon_local"{
 name          = "decathlon.local."
 private_zone  = true
}

resource "aws_efs_file_system" "efs" {
  
  encrypted        = true
  kms_key_id       = "${var.kms_key}" 
  creation_token   = "${var.creation-token}"
  performance_mode = "${var.performance-mode}"
  throughput_mode  = "${var.throughput-mode}"

  tags = {
    Name           = "${var.name}"
    Service        = "efs"
  }
  
   lifecycle {
   ignore_changes         = ["kms_key_id"]
 }
}


resource "aws_efs_mount_target" "efs_az1" {
  file_system_id   = "${aws_efs_file_system.efs.id}"
  subnet_id        = "${var.front_app_subnet_1}"
  security_groups  = ["${var.security_group}"]
}

resource "aws_efs_mount_target" "efs_az2" {
  file_system_id   = "${aws_efs_file_system.efs.id}"
  subnet_id        = "${var.front_app_subnet_2}"
  security_groups  = ["${var.security_group}"]
}


resource "aws_efs_mount_target" "efs_az3" {
  file_system_id   = "${aws_efs_file_system.efs.id}"
  subnet_id        = "${var.front_app_subnet_3}"
  security_groups  = ["${var.security_group}"]
}


###ROUTE53

resource "aws_route53_record" "efs-datasync-local" {
  zone_id = "${data.aws_route53_zone.decathlon_local.zone_id}"
  name    = "${var.prefix}-efs"
  type    = "A"
  ttl     = "300"
  records = ["${aws_efs_mount_target.efs_az1.ip_address}","${aws_efs_mount_target.efs_az2.ip_address}","${aws_efs_mount_target.efs_az3.ip_address}"]
}

