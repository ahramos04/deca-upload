
/*
variable "rds_cluster_arn" {

}
*/
variable "database_name" {

}

variable "engine_version" {

}

variable "source_region" {

}

variable "environment" {

}

variable "backup_retention" {

}

variable "cluster_identifier" {

}

variable "dr_kms_name" {

}

variable "instance_class" {

}

variable "tag_name" {

}


variable "rds_dr_sg_name" {

}

variable "rds_dr_subnet_name" {

}


variable "dr_data_subnet_az1_name" {

}

variable "dr_data_subnet_az2_name" {

}

variable "dr_data_subnet_az3_name" {

}
