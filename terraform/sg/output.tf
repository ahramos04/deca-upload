output "bastion" {
 value = "${aws_security_group.bastion.id}"
}

output "rds_sg" {
 
 value = "${aws_security_group.rds.id}"

}

output "efs_sg" {
 value = "${aws_security_group.efs_sg.id}"
}

output "worker_group_mgmt_one_id" {
  description = "The ID of the SG"
  value       = "${aws_security_group.worker_group_mgmt_one.id}"
}

output "worker_group_mgmt_two_id" {
  description = "The ID of the SG"
  value       = "${aws_security_group.worker_group_mgmt_two.id}"
}

output "all_worker_mgmt_id" {
  description = "The ID of the SG"
  value       = "${aws_security_group.all_worker_mgmt.id}"
}

output "kms_endpoint_sg" {
   value  = "${aws_security_group.kms_endpoint_sg.id}"
}

output "ec2_endpoint_sg" {
   value  = "${aws_security_group.ec2_endpoint_sg.id}"
}

output "alb_sg" {
   value  = "${aws_security_group.alb_sg.id}"
}

output "prestashop_sg" {
   value  = "${aws_security_group.prestashop.id}"
}

output "datasync_sg" {
 value = "${aws_security_group.datasync.id}"
}

output "datasync_sg_arn" {
 value = "${aws_security_group.datasync.arn}"
}

