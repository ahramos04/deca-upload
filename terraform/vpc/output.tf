output "vpc_id" {
 value = "${aws_vpc.vpc.id}"
}

output "public_subnet" {
 value = "${aws_subnet.public_subnet.*.id}"
}

output "public_subnet_az1" {
 value = "${aws_subnet.public_subnet.*.id[0]}"
}

output "data_subnet_az1" {
 value = "${aws_subnet.az1_subnet.*.id[2]}"
}

output "data_subnet_az2" {
 value = "${aws_subnet.az2_subnet.*.id[2]}"
}

output "data_subnet_az3" {
 value = "${aws_subnet.az3_subnet.*.id[2]}"
}

output "front_az1_subnet_arn" {
 value = "${aws_subnet.az1_subnet.*.arn[0]}"
}

output "front_az1_subnet" {
 value = "${aws_subnet.az1_subnet.*.id[0]}"
}

output "front_az2_subnet" {
 value = "${aws_subnet.az2_subnet.*.id[0]}"
}

output "front_az3_subnet" {
 value = "${aws_subnet.az3_subnet.*.id[0]}"
}


output "app_subnet_az1" {
 value = "${aws_subnet.az1_subnet.*.id[1]}"
}

output "app_subnet_az2" {
 value = "${aws_subnet.az2_subnet.*.id[1]}"
}

output "app_subnet_az3" {
 value = "${aws_subnet.az3_subnet.*.id[1]}"
}


output "az1_rt_tbl" {
  value = "${aws_route_table.az1_route.id}"
}

output "az2_rt_tbl" {
  value = "${aws_route_table.az2_route.id}"
}

output "az3_rt_tbl" {
  value = "${aws_route_table.az3_route.id}"
}

