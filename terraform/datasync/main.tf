data "aws_ami" "datasync" {
 name_regex   = "^aws-datasync"  
 owners       = ["633936118553"] 
 most_recent  = true
}




resource "aws_datasync_agent" "example" {
  ip_address = "${aws_instance.datasync.public_ip}"
  name       = "datasync-sg-agent"
}

resource "aws_instance" "datasync" {

 ami                         = "${data.aws_ami.datasync.id}"
 instance_type               = "${var.ec2_instance_type}"
 key_name                    = "${var.ec2_key_name}"
 vpc_security_group_ids      = ["${var.ec2_sg}"]
 subnet_id                   = "${var.ec2_subnet_id}"

 tags{
  Name               = "${var.name}"
  Environment        = "${var.environment}"
 }

 lifecycle {
  ignore_changes = ["instance","user_data"]
  }

}


###DATASYNC LOCATION


resource "aws_datasync_location_efs" "sg_efs" {
  # The below example uses aws_efs_mount_target as a reference to ensure a mount target already exists when resource creation occurs.
  # You can accomplish the same behavior with depends_on or an aws_efs_mount_target data source reference.
  #efs_file_system_arn = "${aws_efs_mount_target.example.file_system_arn}"
  efs_file_system_arn = "${var.efs_mount_arn}"

  ec2_config {
    security_group_arns = ["${var.security_group_arn}"]
    subnet_arn          = "${var.efs_subnet_arn}"
  }
}




resource "aws_datasync_location_nfs" "dr_nfs" {
  server_hostname = "${var.dr_efs_hostname}"
  subdirectory    = "/"

  on_prem_config {
    agent_arns = ["${aws_datasync_agent.example.arn}"]
  }
}


###DATASYNC TASK



resource "aws_datasync_task" "datasync_task" {
  destination_location_arn = "${aws_datasync_location_nfs.dr_nfs.arn}"
  name                     = "efs datasync to dr"
  source_location_arn      = "${aws_datasync_location_efs.sg_efs.arn}"

  options {
    bytes_per_second = -1
  }
}




