output "external_alb_dns_name" {
 value = "${aws_lb.external_alb.dns_name}"
}

output "external_alb_zone_id" {
 value = "${aws_lb.external_alb.zone_id}"
}


output "external_alb" {
 value = "${aws_lb.external_alb.arn}"
}




/*
output "nlb_dns_name" {
 value = "${aws_lb.internal_nlb.dns_name}"
}

output "nlb_zone_id" {
 value = "${aws_lb.internal_nlb.zone_id}"
}


output "internal_nlb" {
 value = "${aws_lb.internal_nlb.arn}"
}
*/
