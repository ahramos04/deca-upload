#!/bin/bash
set -x
exec > >(tee /var/log/user-data.log|logger -t user-data ) 2>&1
echo BEGIN
date '+%Y-%m-%d %H:%M:%S'

#logdir=/var/log
#logfile=$logdir/efs_mount.log
#exec >> $logfile 2>&1


#Install Amazon EFS Utils
sudo yum install amazon-efs-utils -y


##Mount efs drive to /opt/efs
DIR_EFS='/opt/efs'

if [ -e $DIR_EFS ]; then
  sudo mount -t efs "${efs_id}":/ /opt/efs && sudo echo "${efs_id}:/ /opt/efs efs defaults,_netdev 0 0" | sudo tee -a /etc/fstab && \
  echo "${efs_id}:/img /var/www/html/img efs defaults,_netdev 0 0" | sudo tee -a /etc/fstab && sudo mount -a

else 
  sudo mkdir /opt/efs && sudo mount -t efs "${efs_id}":/ /opt/efs && sudo echo "${efs_id}:/ /opt/efs efs defaults,_netdev 0 0" | sudo tee -a /etc/fstab && \
  echo "${efs_id}:/img /var/www/html/img efs defaults,_netdev 0 0" | sudo tee -a /etc/fstab && sudo mount -a  

fi 

#echo "${efs_id}:/img /var/www/html/img efs defaults,_netdev 0 0" | sudo tee -a /etc/fstab && sudo mount -a
echo "${efs_id}:/img /var/www/html/img efs defaults,_netdev 0 0" | sudo tee -a /etc/fstab && \
echo "${efs_id}:/modules/iqitthemeeditor/views/css /var/www/html/modules/iqitthemeeditor/views/css efs defaults,_netdev 0 0" | sudo tee -a /etc/fstab && \
echo "${efs_id}:/modules/iqitthemeeditor/views/js /var/www/html/modules/iqitthemeeditor/views/js efs defaults,_netdev 0 0" | sudo tee -a /etc/fstab && \
echo "${efs_id}:modules/amazzingfilter/indexes /var/www/html/modules/amazzingfilter/indexes efs defaults,_netdev 0 0" | sudo tee -a /etc/fstab && \
echo "${efs_id}:/var /var/www/html/var efs defaults,_netdev 0 0" | sudo tee -a /etc/fstab && sudo mount -a




#Create a file parameters.php 

echo "\
<?php return array (
  'parameters' => 
  array (
    'database_host' => '${db_host}',
    'database_port' => '',
    'database_name' => 'presta',
    'database_user' => '${db_user}',
    'database_password' => '${db_password}',
    'database_prefix' => 'de_',
    'database_engine' => 'InnoDB',
    'mailer_transport' => 'smtp',
    'mailer_host' => '127.0.0.1',
    'mailer_user' => NULL,
    'mailer_password' => NULL,
    'secret' => 'Jy0vadRAcWHZELqzG71jrsKitqvdYPph59eaF10Bh6fy77ouh10txQC8',
    'ps_caching' => 'CacheMemcache',
    'ps_cache_enable' => false,
    'ps_creation_date' => '2018-08-28',
    'locale' => 'en-US',
    'cookie_key' => 'KUZ38B4Ajk7AK8nXqnoMnraH3bP8AZbhKPv6tE3DMiHB4X41aW2YLS9K',
    'cookie_iv' => 'sAjieyGh',
    'new_cookie_key' => 'def000000f38f0b9ecc052016b8b3c93d2b31ab4ec166050b8dffe9d01a1165cac520eed3752183b9c8b6e6fa20978eba30fe4b75f03feea50787a1cf674ba72f69b462c',
  ),
);
" | tee /home/ec2-user/parameters.php

if [ -e /home/ec2-user/parameters.php ]; then
  sudo cp /home/ec2-user/parameters.php /var/www/html/app/config/parameters.php && sudo chown apache: /var/www/html/app/config/parameters.php && curl -L https://www.decathlon.sg/module/amazzingfilter/cron?token=e9d0da1474f81fce72af06b133965509&id_shop=1&action=index-all

fi


